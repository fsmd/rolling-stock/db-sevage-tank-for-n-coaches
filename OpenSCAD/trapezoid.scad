/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
// Trapezoid
module trapezoid(size, center = false) {

    if(center) {
        translate([-size[0] / 2, -size[1] / 2, -size[4] / 2])
        trapezoid_offcenter();
    }
    else {
        trapezoid_offcenter();
    }
    
    module trapezoid_offcenter() {
        polyhedron(
            points = 
                [[0, 0, 0], 
                [size[0], 0, 0], 
                [size[0], size[1], 0], 
                [0, size[1], 0], 
                [(size[0] - size[2]) / 2, (size[1] - size[3]) / 2, size[4]], 
                [(size[0] - size[2]) / 2 + size[2], (size[1] - size[3]) / 2, size[4]], 
                [(size[0] - size[2]) / 2 + size[2], (size[1] - size[3]) / 2 + size[3], size[4]], 
                [(size[0] - size[2]) / 2, (size[1] - size[3]) / 2 + size[3], size[4]]], 
            faces = [
                [0, 1, 2, 3], 
                [4, 5, 1, 0], 
                [5, 6, 2, 1], 
                [6, 7, 3, 2], 
                [7, 4, 0, 3], 
                [7, 6, 5, 4]]
        ); 
    }
}