/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Abwasserbehälter nach Zeichnung 1.0150.97.00.001 für n-Wagen
 * Sevage tank according to drawing 1.0150.97.00.001 for n-coaches
 *
 * Ressources/Quellen
 * [1]: Eigenes Aufmaß
 * [2]: Zeichnung 1.0150.97.00.001
 */


// allgemeine Komponenten
include <hexagon.scad>
include <octagon.scad>
include <trapezoid.scad>
include <rcube.scad>

faktor = 1 / 87; // Maßstab


// Maße in mm

// allgemeine Maße
x0 = -365; // Behälterhinterkante
x1 = 45; // Behälterzwischenkante
x2 = 365; // Behältervorderkante
y0 = -1050; // Behälterkante hinten links (von vorne)
y1 = -y0; // Behälterkante hinten rechts (von vorne)
y3 = -623;  // Behälterkante vorne links (von vorne)
y2 = -y3; // Behälterkante vorne rechts (von vorne)
z0 = -235; // Behälterunterkante
z1 = 115; // Behälterkante mittleres Niveau
z2 = 235; // Behälteroberkante
d_min = 0.25; // Mindeststärke freie Teile anhand Fertigungsmöglichkeiten ELEGOO Mars
ok = z0 + (9.2 - 0.5) / faktor; // Oberkante inkl. Montageplatte unter Wagenboden

// Bodendeckel
bodendeckel_l = 184; // Länge
bodendeckel_b = 502 * 2; // Länge
bodendeckel_h = (48 + 2) - (40 + 3 + 2); // Länge
bodendeckel_x = 0 + bodendeckel_l / 2; // x-Position ggü. Nullpunkt
bodendeckel_y = 0; // y-Position ggü. Nullpunkt
bodendeckel_z = z0 + bodendeckel_h / 2 * 0; // z-Position ggü. Nullpunkt

// Montage
$fn = $preview == true ? 64 : 128;

scale(faktor) {
    union() {
        difference() {
            // Behälter
            behaelter();

            // Bodendeckel
            translate([bodendeckel_x, 0, bodendeckel_z]) cube([bodendeckel_l, bodendeckel_b, bodendeckel_h], true);
        }

        // Abdeckplatte unten
        abdeckplatte();

        // Schrauben am Bodendeckel
        translate([bodendeckel_x, 0, bodendeckel_z]) bodendeckel_schrauben();

        // Absaugstutzen
        absaugstutzen("links");
        absaugstutzen("rechts");

        // Anschlusskasten
        anschlusskasten();

        // Aufhängungen
        aufhaengung("rechts", "gerade");
        aufhaengung("links", "gerade");
        aufhaengung("rechts", "gewinkelt");
        aufhaengung("links", "gewinkelt");

        // Aufsatz
        aufsatz();

        // Belüftungsrohr
        belueftung();

        // Erdungsanschluss
        erdung();

        // Mannloch
        mannloch();

        // WC-Zulauf
        wc_zulauf();

        // Grundplatte
        #montageplatte();
    }
}

// Behälter
module behaelter() {
    /*
     * Abwasserbehälter 350 l
     */

    #polyhedron(
        points = [
            [x0, y0, z0], [x0, y1, z0], [x1, y1, z0], [x1, y2, z0], [x2, y2, z0], [x2, y3, z0], [x1, y3, z0], [x1, y0, z0],
            [x0, y0, z1], [x0, y3, z1], [x1, y3, z1], [x1, y0, z1], 
            [x0, y3, z2], [x0, y1, z2], [x1, y1, z2], [x1, y2, z2], [x2, y2, z2], [x2, y3, z2]
        ], 
        faces = [
            [7, 6, 5, 4, 3, 2, 1, 0], 
            [8, 9, 10, 11],
            [12, 13, 14, 15, 16, 17],
            [0, 1, 13, 12, 9, 8],
            [1, 2, 14, 13],
            [2, 3, 15, 14],
            [3, 4, 16, 15],
            [4, 5, 17, 16],
            [5, 6, 10, 9, 12, 17],
            [6, 7, 11, 10],
            [7, 0, 8, 11]
        ],
        convexity = 10
    );
}

// Abdeckplatte unten
module abdeckplatte() {
    /*
     * Runde Abdeckplatte an der Unterseite des Behälters
     */

    abdeckplatte_d = 76.1; // Durchmesser
    abdeckplatte_h = 5; // Höhe
    abdeckplatte_x = -75; // x-Position ggü. Nullpunkt
    abdeckplatte_y = 300; // y-Position ggü. Nullpunkt
    abdeckplatte_z = z0 - abdeckplatte_h / 2; // z-Position ggü. Nullpunkt

    translate([abdeckplatte_x, abdeckplatte_y, abdeckplatte_z]) cylinder(h = abdeckplatte_h, r = abdeckplatte_d / 2, center = true);    
}

// Aufsatz
module aufsatz() {
    /*
     * Aufsatz mit Schrauben
     */

    aufsatz_l = 190; // Länge
    aufsatz_b = 170 * 2; // Breite
    aufsatz_h = 67 - 10; // Höhe
    aufsatz_x = -55 - aufsatz_l / 2; // x-Position ggü. Nullpunkt
    aufsatz_z = z2 + aufsatz_h / 2; // z-Position ggü. Nullpunkt
    aufsatz_schraube_k = 5.3; // Kopfhöhe
    aufsatz_schraube_s = 13; // Schlüsselweite
    aufsatz_schraube_x = aufsatz_l / 2 - 20; // x-Position ggü. Nullpunkt
    aufsatz_schraube_y = aufsatz_b / 2 - 20; // y-Position ggü. Nullpunkt
    aufsatz_schraube_z = (aufsatz_h + aufsatz_schraube_k) / 2; // z-Position ggü. Nullpunkt

    translate([aufsatz_x, 0, aufsatz_z]) {
        cube([aufsatz_l, aufsatz_b, aufsatz_h], true);
        for (x = [-aufsatz_schraube_x : (aufsatz_schraube_x * 2 / 2) : aufsatz_schraube_x]) {
            for (y = [-aufsatz_schraube_y : (aufsatz_schraube_y * 2 / 4) : aufsatz_schraube_y]) {
                if (x != 0 || round(abs(y)) == aufsatz_schraube_y) {
                    beta = rands(0, 60, 6)[0]; // ich möchte abhängig von der Montageseite die Ausrichtung ändern
                    translate([x, y, aufsatz_schraube_z]) rotate([0, 0, beta]) hexagon(aufsatz_schraube_s, aufsatz_schraube_k, true);
                }
            }
        }
    }
}

// Absaugstutzen
module absaugstutzen(position) {
    /*
     * Stutzen zur Absaugung des Abwassers mit Deckel
     */

    absaugstutzen_d = 60.3; // Durchmesser Rohr
    absaugstutzen_y_l = y0; // y-Position ggü. Nullpunkt
    absaugstutzen_y_r = -absaugstutzen_y_l; // y-Position ggü. Nullpunkt
    absaugstutzen_z = 72; // z-Position ggü. Nullpunkt
    absaugstutzen_alpha = 15.5; // Winkel ggü. Horizontalen
    absaugstutzen_l1 = (115 - absaugstutzen_z) / tan(absaugstutzen_alpha); // Länge schräger Teil horizontal gemessen
    absaugstutzen_l2 = 2492 / 2 - abs(absaugstutzen_y_l) - absaugstutzen_l1; // Länge waagerechter Teil
    absaugstutzen_x_l = -115; // x-Position ggü. Nullpunkt
    absaugstutzen_x_r = -185; // x-Position ggü. Nullpunkt


    // Schlauchanschluss
    mutter_s = 100; // Schlüsselweite
    mutter_k = 22; // Kopfhöhe

    inv = position == "links" ? 0 : 1; // ich möchte abhängig von der Montageseite die Ausrichtung ändern

    // Absaugstutzen schräger Teil
    translate([absaugstutzen_x_l * (1 - inv) + absaugstutzen_x_r * inv, absaugstutzen_y_l * (1 - inv) + absaugstutzen_y_r * inv, absaugstutzen_z]) rotate([0, 0, inv * 180]) translate([0, -absaugstutzen_l1 / 2, (absaugstutzen_l1 / 2) * tan(absaugstutzen_alpha)]) {
        l1_estimate = 15; // Länge; ACHTUNG: geschätzt
        d1_estimate = 80; // Durchmesser; ACHTUNG: geschätzt
        translate([0, -absaugstutzen_l1 / 2 - (absaugstutzen_l2 - l1_estimate), (absaugstutzen_l1 / 2) * tan(absaugstutzen_alpha)]) rotate([90, 0, 0]) cylinder(r = d1_estimate / 2, h = l1_estimate, center = false);
        
        translate([0, -absaugstutzen_l1 / 2, (absaugstutzen_l1 / 2) * tan(absaugstutzen_alpha)]) rotate([90, 0, 0]) cylinder(r = absaugstutzen_d / 2, h = absaugstutzen_l2, center = false);
        intersection() {
            rotate([-absaugstutzen_alpha, 0, 0]) rotate([90, 0, 0]) {
                cylinder(r = absaugstutzen_d / 2, h = absaugstutzen_l1 * 2, center = true);

                // Kettenbefestigung
                l2_estimate = 55.25; // Länge ab Mittelachse; ACHTUNG: geschätzt
                translate([0, -l2_estimate / 2, absaugstutzen_l1 / 2 - 40]) cube([d_min / faktor, l2_estimate, d_min / faktor] , true);
            }
            cube([absaugstutzen_d, absaugstutzen_l1, absaugstutzen_l1 * tan(absaugstutzen_alpha) + absaugstutzen_d / cos(absaugstutzen_alpha)], true);
        }

        /*
         * ACHTUNG: Maße ab hier geschätzt
         */
        translate([0, -absaugstutzen_l1 / 2 - absaugstutzen_l2 + l1_estimate / 2, (absaugstutzen_l1 / 2) * tan(absaugstutzen_alpha)]) rotate([90, rands(-10, 10, 6)[0], 0]) {
            // Achtkantmutter
            intersection() {
                octagon(mutter_s, mutter_k);
                hull() {
                    cylinder(r = mutter_s / cos(45 / 2) / 2, h = mutter_k / 5 * 3, center = true);
                    cylinder(r = mutter_s / 2, h = mutter_k, center = true);
                }
            }

            // Kappe für Anschlussstutzen
            kappe_h = 50; // Höhe
            translate([0, 0, (mutter_k + kappe_h) / 2]) {
                cylinder(r = mutter_s / 2, h = kappe_h, center = true);
                translate([0, 0, (kappe_h - 16) / 2]) cylinder(r = mutter_s / cos(45 / 2) / 2, h = 16, center = true);
                translate([0, 0, (16 - kappe_h) / 2]) cylinder(r = mutter_s / cos(45 / 2) / 2, h = 16, center = true);

                rotate([90, 0, 0]) rcube([140, kappe_h - 10, 40], [(kappe_h - 10) / 2, (kappe_h - 10) / 2, (kappe_h - 10) / 2, (kappe_h - 10) / 2], true);

                // Kettenbefestigung
                hull() {
                    translate([0, 0, (kappe_h + d_min / faktor) / 2]) rotate([90, 90, 0]) cylinder(r = d_min / faktor / 2, h = d_min / faktor, center = true);
                    rotate([90, 90, 0]) cylinder(r = d_min / faktor / 2, h = d_min / faktor, center = true);
                }
            }

            // Befestigungsbügel
            buegel_l = kappe_h * 1.5; // Länge
            translate([(140 - d_min / faktor) / 2, 0, buegel_l - 20]) rotate([0, 90, 90]) rcube([buegel_l, d_min / faktor, d_min / faktor], [5, 10, 0, 5], true);
            translate([(d_min / faktor - 140) / 2, 0, buegel_l - 20]) rotate([0, 90, 270]) rcube([buegel_l, d_min / faktor, d_min / faktor], [5, 10, 0, 5], true);
        }
    }
}

// Anschlusskasten
module anschlusskasten() {
    /*
     * Elektroanschlusskasten
     * To do:
     * - Kabel hinzufügen
     * - weitere Details prüfen
     */

    anschlusskasten_l = 200; // Länge
    anschlusskasten_b = 1050 - 623 - 373; // Breite
    anschlusskasten_h = 235 - 50 + 117; // Höhe
    anschlusskasten_x = 85 + anschlusskasten_l / 2; // x-Position ggü. Nullpunkt
    anschlusskasten_y = y3 -anschlusskasten_b / 2; // y-Position ggü. Nullpunkt
    anschlusskasten_z = -117 + anschlusskasten_h / 2; // z-Position ggü. Nullpunkt

    translate([anschlusskasten_x, anschlusskasten_y, anschlusskasten_z]) rotate([0, 90, 90]) rcube([anschlusskasten_h, anschlusskasten_l, anschlusskasten_b], [10, 10, 10, 10], true);    
}

// Aufhängung
module aufhaengung(position, typ) {
    /*
     * Aufhängung für Abweassertank in zwei verschiedenen Typen
     */

    r = 12.5; // Radius
    l = 120; // Länge
    b = 100; // Breite
    h = typ == "gewinkelt" ? 116 + 2 * r : 180 + 115 - 235;
    d = d_min / faktor; // Dicke; angepasst an Fertigungsmöglichkeiten von ELEGOO Mars
    x = 0; // x-Position ggü. Nullpunkt
    aufhaengung_x = typ == "gewinkelt" ? 330 - 85 + l / 2 : -310 + 85 - l / 2; // x-Position ggü. Nullpunkt
    aufhaengung_y = typ == "gewinkelt" ? 610 : 487.5; // x-Position ggü. Nullpunkt
    aufhaengung_z = 115 + 180; // x-Position ggü. Nullpunkt

    inv = position == "links" ? 1 : -1; // ich möchte abhängig von der Montageseite die Ausrichtung ändern

    translate([aufhaengung_x, aufhaengung_y * -inv, aufhaengung_z]) rotate([0, 90, 0]) linear_extrude(height = l, center = true) translate([r, 0, 0]) difference() {
        aufhaengung_grundform();
        offset(r = -d) aufhaengung_grundform();
    }

    // Grundformen
    module aufhaengung_grundform() {
        hull() {
            translate([0, b / 2 - r, 0]) circle(r = r);
            translate([0, r - b / 2, 0]) circle(r = r);
            if (typ == "gewinkelt") {
                translate([14, inv * (r - b / 2), 0]) circle(r = r);
                translate([116, inv * (b / 2 - r)]) circle(r = r);
            }
            else {
                translate([h, inv * (r - b / 2), 0]) circle(r = r);
                translate([h, inv * (b / 2 - r)]) circle(r = r);
            }
        }
    }
}

// Belüftungsrohr
module belueftung() {
    /*
     * Belüftungsrohr umgekehrtes U
     * Befestigungswinkel ist angepasst an Fertigungsmöglichkeiten von ELEGOO Mars als Quader dargestellt
     */

    belueftung_d = 33.7; // Durchmesser
    belueftung_winkel_y = 300; // y-Position ggü. Nullpunkt
    belueftung_winkel_z = z2 + 38; // z-Position ggü. Nullpunkt
    belueftung_winkel_1_x = -215; // x-Position ggü. Nullpunkt
    belueftung_winkel_2_x = -392; // x-Position ggü. Nullpunkt
    belueftung_rohr_l = belueftung_winkel_z - z0; // Länge senkrechtes Rohr
    belueftung_rohr_z = z0 + belueftung_rohr_l / 2;
    belueftung_befestigung_l = abs(belueftung_winkel_2_x - x0); // Länge Befestigungswinkel
    belueftung_befestigung_b = belueftung_befestigung_l; // Breite Befestigungswinkel
    belueftung_befestigung_h = belueftung_befestigung_l; // Höhe Befestigungswinkel
    belueftung_befestigung_x = belueftung_winkel_2_x + belueftung_befestigung_l / 2; // x-Position ggü. Nullpunkt
    belueftung_befestigung_y = belueftung_winkel_y + belueftung_befestigung_b / 2; // x-Position ggü. Nullpunkt
    belueftung_befestigung_z = z0 + 40 + belueftung_d / 2; // z-Position ggü. Nullpunkt

    translate([0, belueftung_winkel_y, belueftung_winkel_z]) {
        translate([belueftung_winkel_1_x, 0, 0]) belueftung_winkel(belueftung_d);
        translate([belueftung_winkel_2_x, 0, 0]) belueftung_winkel(belueftung_d);
    }
    translate([(belueftung_winkel_2_x + belueftung_winkel_1_x) / 2, belueftung_winkel_y, belueftung_winkel_z]) rotate([0, 90, 0]) linear_extrude(height = abs(belueftung_winkel_2_x - belueftung_winkel_1_x), center = true) circle(d = belueftung_d);
    translate([0, belueftung_winkel_y, belueftung_rohr_z]) {
        translate([belueftung_winkel_1_x, 0, 0]) cylinder(r = belueftung_d / 2, h = belueftung_rohr_l, center = true);
        translate([belueftung_winkel_2_x, 0, 0]) cylinder(r = belueftung_d / 2, h = belueftung_rohr_l, center = true);
    }
    translate([belueftung_befestigung_x, belueftung_befestigung_y, belueftung_befestigung_z]) cube([belueftung_befestigung_l, belueftung_befestigung_b, belueftung_befestigung_h], true);

    // Belüftungsrohr-Verbinder
    module belueftung_winkel(diameter) {
        /*
        * Verbindet zwei Rohre im Winkel von 90 °
        */
        
        intersection() {
            linear_extrude(height = diameter, center = true) circle(d = diameter);
            rotate([0, 90, 0]) linear_extrude(height = diameter, center = true) circle(d = diameter);
        }
    }
}

// Bodendeckel Schrauben
module bodendeckel_schrauben() {
    /*
     * Schrauben für Bodendeckel
     */

    bodendeckel_schraube_offset = 14; // Abstand Schraubenmitte vom Rand des Deckels
    bodendeckel_schraube_k = 5.3; // Kopfhöhe
    bodendeckel_schraube_s = 13; // Schlüsselweite
    bodendeckel_schraube_x = bodendeckel_l / 2 - bodendeckel_schraube_offset; // x-Position ggü. Nullpunkt
    bodendeckel_schraube_y = bodendeckel_b / 2 - bodendeckel_schraube_offset; // y-Position ggü. Nullpunkt
    bodendeckel_schraube_z = bodendeckel_h / 2 - bodendeckel_schraube_k / 2; // z-Position ggü. Nullpunkt

    for (x = [-bodendeckel_schraube_x : (bodendeckel_schraube_x * 2 / 2) : bodendeckel_schraube_x]) {
        for (y = [-bodendeckel_schraube_y : (bodendeckel_schraube_y * 2 / 9) : bodendeckel_schraube_y]) {
            if (x != 0 || round(abs(y)) == bodendeckel_schraube_y) {
                beta = rands(0, 60, 6)[0]; // ich möchte den Winkel der Schrauben zufällig einstellen
                translate([x, y, bodendeckel_schraube_z]) rotate([0, 0, beta]) hexagon(bodendeckel_schraube_s, bodendeckel_schraube_k, true);
            }
        }
    }
}

// Erdungsanschluss
module erdung() {
    /*
     * Funktion unklar, evtl. Erdungsanschluss?
     * ACHTUNG: Maße geschätzt
     */

    erdung_l = d_min / faktor; // Länge; ACHTUNG: geschätzt
    erdung_b = 40; // Breite; ACHTUNG: geschätzt
    erdung_h = 30; // Höhe; ACHTUNG: geschätzt
    erdung_x = -55; // x-Position ggü. Nullpunkt
    erdung_y = y0 - erdung_b / 2; // y-Position ggü. Nullpunkt
    erdung_z = z1 - 10 - erdung_h / 2; // z-Position ggü. Nullpunkt

    translate([erdung_x, erdung_y, erdung_z]) cube([erdung_l, erdung_b, erdung_h] , true);
}

// Mannloch
module mannloch() {
    /*
     * Mannlochabdeckung in Front mit Schrauben
     */

    mannloch_deckel_d = 300; // Durchmesser
    mannloch_deckel_h = 14; // Höhe
    mannloch_deckel_x = x2 + mannloch_deckel_h / 2; // x-Position ggü. Nullpunkt
    mannloch_deckel_y = 270; // y-Position ggü. Nullpunkt
    mannloch_schraube_k = 5.3; // Kopfhöhe
    mannloch_schraube_s = 13; // Schlüsselweite
    mannloch_schraube_x = (mannloch_deckel_h + mannloch_schraube_k) / 2; // x-Position ggü. Nullpunkt
    mannloch_schraube_z = mannloch_deckel_d / 2 - 20; // z-Position ggü. Nullpunkt

    translate([mannloch_deckel_x, mannloch_deckel_y, 0]) union() {
        // Platte
        rotate([0, 90, 0]) cylinder(mannloch_deckel_h, mannloch_deckel_d / 2, mannloch_deckel_d / 2, true);

        // Schrauben
        for ( alpha = [0 : 45 : 315] ) {
            beta = rands(0, 60, 6)[0]; // ich möchte den Winkel der Schrauben zufällig einstellen
            rotate([alpha, 0, 0]) translate([mannloch_schraube_x, 0, mannloch_schraube_z]) rotate([beta, 0, 0]) rotate([0, 90, 0]) hexagon(mannloch_schraube_s, mannloch_schraube_k, true);
        }
    }
}

// Montageplatte
module montageplatte() {
    /*
     * Montageplatte zur Befestigung am Brawa-Wagenboden
     * relevante Maße wurden am Modell abgenommen
     *
     * Gesamtkonstruktion von Brawa hat eine Länge von 7,2 mm (ohne Prallplatte)
     * To do:
     * - Position hinterfragen, da Brawa-Bauteil nicht maßstäblich!
     * - Anprallschutz für Kupplung
     *
     * Unterkante bei Brawa ca 2,7 mm über SOK > 235 mm Vorbild
     * Vorbild dagegen 250 bis 350 mm über SOK > 2,9 ... 4.0
     * Maß OK daher um 0,5 mm reduziert (siehe oben), damit reduziert sich der Abstand 
     * des Behälters zur Montageplatte und der Behälter sitzt im Ergebnis höher
     */

    montageplatte_b1 = 29.75 / faktor; // Breite 1
    montageplatte_b2 = 29.35 / faktor; // Breite 2
    montageplatte_l = 4.6 / faktor; // Länge
    montageplatte_h = 0.55 / faktor; // Dicke
    montageplatte_x = x2 - 7.16 / faktor + montageplatte_l / 2;// - montageplatte_l / 2; // x-Position ggü. Nullpunkt
    montageplatte_z = ok - montageplatte_h / 2; // z-Position ggü. Nullpunkt

    translate([montageplatte_x, 0, montageplatte_z]) {
        // Platte unter Wagenboden
        rotate([90, 0, 90]) trapezoid([montageplatte_b1, montageplatte_h, montageplatte_b2, montageplatte_h, montageplatte_l], true);

        // Montagezapfen rund
        montageplatte_zapfen_d = 1.2 / faktor; // Durchmesser
        montageplatte_zapfen_h = 2 / faktor; // Höhe
        montageplatte_zapfen_x = (montageplatte_zapfen_d - montageplatte_l) / 2; // x-Position ggü. Nullpunkt
        montageplatte_zapfen_y = montageplatte_b1 / 2 - 2.5 / faktor; // y-Position ggü. Nullpunkt
        montageplatte_zapfen_z = (montageplatte_h + montageplatte_zapfen_h) / 2; // z-Position ggü. Nullpunkt

        translate([montageplatte_zapfen_x, montageplatte_zapfen_y, montageplatte_zapfen_z]) hull() {
            translate([0, 0, -0.2 / faktor]) cylinder(r = montageplatte_zapfen_d / 2, h = (2 / faktor) - 0.2 / faktor, center = true);
            cylinder(r = montageplatte_zapfen_d / 2 - 0.3 / faktor, h = (2 / faktor), center = true);
        }
        
        // Montagenase eckig
        gundplatte_nase_l = 2 / faktor; // Länge
        gundplatte_nase_b = 1 / faktor; // Breite
        gundplatte_nase_h = montageplatte_zapfen_h; // Höhe
        gundplatte_nase_x = (gundplatte_nase_l - montageplatte_l) / 2 + 0.4 / faktor; // x-Position ggü. Nullpunkt
        gundplatte_nase_y = montageplatte_zapfen_y - (montageplatte_zapfen_d + gundplatte_nase_b) / 2 - 25 / faktor; // y-Position ggü. Nullpunkt
        gundplatte_nase_z = (montageplatte_h + gundplatte_nase_h) / 2; // z-Position ggü. Nullpunkt

        translate([gundplatte_nase_x, gundplatte_nase_y, gundplatte_nase_z]) hull() {
            translate([0, 0, -0.2 / faktor]) cube([gundplatte_nase_l, gundplatte_nase_b, gundplatte_nase_h - 0.2 / faktor], true);
            cube([gundplatte_nase_l - 0.6 / faktor, gundplatte_nase_b - 0.6 / faktor, gundplatte_nase_h], true);
        }
    }

    aufhaengung_hilfe();

    // Hilfskonstruktion zur Aufhängung
    module aufhaengung_hilfe() {
        /*
         * To Do: Klären, wie das in Echt aussieht
         */
        l = 120; // Länge
        b = 100; // Breite
        h = montageplatte_z - (115 + 180) - montageplatte_h / 2; // Höhe
        z = 115 + 180 + h / 2; // z-Position ggü. Nullpunkt

        translate([0, 0, montageplatte_z]) cube([730, 15.2 / faktor, montageplatte_h], true);
        translate([330 - 85 + l / 2, 610, z]) cube([l, b, h], true);
        translate([330 - 85 + l / 2, -610, z]) cube([l, b, h], true);
        translate([-310 + 85 - l / 2, 487.5, z]) cube([l, b, h], true);
        translate([-310 + 85 - l / 2, -487.5, z]) cube([l, b, h], true);
    }
}

// WC-Zulauf
module wc_zulauf() {
    /*
     * Zulauf zum Abwassertank vom WC her
     * Annahme: Gesamtdurchmesser 100 mm
     */

    wc_d1 = 48.3; // Durchmesser Stutzen
    wc_d2 = wc_d1 + 2 * 25.85; // Durchmesser inkl. Isolierung
    wc_h1 = 48; // Höhe Stutzen
    wc_h2 = ok - z2; // Höhe bis Montageplatte
    wc_x = -165; // x-Position ggü. Nullpunkt
    wc_y = 950; // y-Position ggü. Nullpunkt
    wc_z = z2; // z-Position ggü. Nullpunkt        
    wc_alpha = 40; // Winkel im Versatz
    wc_beta = atan((205.72 + wc_x) / (1076.63 - wc_y)); // Winkel in der Aufsicht ggü. der Horizontalen
    wc_r = 90; // Innenradius im Versatz
    wc_l0 = 133; // Abstand der beiden Rohrachsen in der Aufsicht
    wc_l1 = 20; // Länge 1. gerader Abschnitt
    wc_l2 = (wc_l0 - ((wc_d1 + wc_r) - (wc_d1 + wc_r) * cos(wc_alpha)) * 2) / sin(wc_alpha); // Länge 2. gerader Abschnitt
    wc_l3 = wc_h2; // Länge 3. gerader Abschnitt

    // Rohr inkl. Isolierung
    translate([wc_x, wc_y, wc_z]) {
        // Stutzen
        cylinder(r = wc_d1 / 2, h = wc_h1, center = false);

        rotate([0, 0, wc_beta]) intersection() {
            union() {
                cylinder(r = wc_d2 / 2, h = wc_l1, center = false);
                translate([0, 0, wc_l1]) rotate([0, 0, 180]) {
                    wc_bend(wc_d2, wc_alpha, wc_r);
                    translate([0, -(wc_r + wc_d2 / 2), 0]) rotate([wc_alpha, 0, 0]) {
                        translate([0, wc_r + wc_d2 / 2, 0]) cylinder(r = wc_d2 / 2, h = wc_l2, center = false);
                        translate([0, wc_r + wc_d2 / 2, wc_l2]) rotate([0, 0, 180]) {
                            wc_bend(wc_d2, wc_alpha, wc_r);
                            translate([0, -(wc_r + wc_d2 / 2), 0]) rotate([wc_alpha, 0, 0]) translate([0, wc_r + wc_d2 / 2, 0]) cylinder(r = wc_d2 / 2, h = wc_l3, center = false);
                        }
                    }
                }
            }
            // ich möchte die Höhe bis OK Montageplatte begrenzen
            translate([0, (350 - wc_d2) / 2, wc_h2 / 2]) cube([wc_d2, 350, wc_h2], true);
        }
    }

    // Bogen einzeln
    module wc_bend(diameter, angle, radius) {
        rotate([90, 0, 90]) translate([-(radius + diameter / 2), 0, 0]) rotate_extrude(angle = angle, convexity = 10) translate([radius + diameter / 2, 0, 0]) circle(d = diameter);
    }
}