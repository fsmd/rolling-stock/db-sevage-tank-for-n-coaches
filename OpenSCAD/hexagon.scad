/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
// Idea: https://www.youtube.com/watch?v=KAKEk1falNg

// Sechseck
module hexagon(width, height, center = false)
{
    offset = (width / 2) / sin(60);
    
    if(center == false) {
        translate([offset, width / 2, height /2])
        hexagon_center();
    }
    else {
        hexagon_center();
    }

    module hexagon_center() {
        union() {
            for ( alpha = [0 : 60 : 120] ) {
                rotate([0, 0, alpha]) cube([offset, width, height], true);
            }
        }
    }
}